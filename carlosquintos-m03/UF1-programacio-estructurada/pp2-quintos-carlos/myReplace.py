"""
   Carlos Quintos Nores
   12 de Des. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Programa que implementi la funcionalitat del mètode replace.
   És a dir un programa que demani una cadena de caràcters, un
   caràcter a modificar i un alre caràcter de substitució.
   El programa haurà de mostrar com a resultat la cadena amb tots
   els caràcters a modificar i un altre caràcter de susbstitució.
"""

# region Variables & Constants
fraseInput = input("Introdueix una frase:\n")
# endregion

# region Main Program
replace = input("\nQuin caràcter vols reemplaçar?:\n")
substitut = input("\nQuien és el caràcter de susbstitució?:\n")
for lletra in fraseInput:
   if lletra == replace:
      print(substitut, end="")
      continue
   print(fraseInput)
# endregion