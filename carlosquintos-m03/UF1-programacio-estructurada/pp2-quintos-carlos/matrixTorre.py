"""
    Carlos Quintos Nores
    12 de Des. 2022
    ASIXc1C M03 UF1 A4
    Descripció:
    Programa que pinta per pantalla un taulell d'escacs marcat amb
    una B les caselles blanques i amb una N les negres.
    El programa. haurà de marcar amb * les caselles a les quals
    pots moureś una torre des d'una posició concreta que cal
    demanar a l'usuari per al terminal.
"""

# region Variables & Constants
B = "B"
N = "N"
TORRE = "*"
FILES = 8
# endregion

# region Main Program
for i in range (0, FILES):
    for j in range(0, FILES):
        if i % 2 == 0:
            j += 1
        if j % 2 == 0:
            print(N, end=' ')
        else:
            print(B, end=' ')
    print ()
# endregion