"""
    Carlos Quintos Nores
    12 de Des. 2022
    ASIXc1C M03 UF1 A4
    Descripció:
    L'usuari introduirà una llista d'enter separada per espais.
    Prèviament dirà quants enters introduirà.
    Un cop llegits tots, mostrarà per pantalla la suma de tots
    els valors positius.
"""

# region Main Program
numInput = input("Introdueix enters(5): ")
splitter = numInput.split(" ")
num1 = int(splitter[0])
num2 = int(splitter[1])
num3 = int(splitter[2])
num4 = int(splitter[3])
num5 = int(splitter[4])
suma = num1+num2+num3+num4+num5
print(suma)
# endregion