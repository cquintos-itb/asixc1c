"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   Introdueix el número de persones i el número
   de galetes.
   Si a tothom li toquen el mateix número de galetes
   imprimeix "Let's Eat!", sinó imprimeix "Let's Fight".
"""

nPersones = int(input("Introdueix número de Persones: "))
nGaletes = int(input("Introdueix número de Galetes: "))
if nGaletes % nPersones == 0:
    print("Let's Eat!")
else:
    print("Let's Fight!")