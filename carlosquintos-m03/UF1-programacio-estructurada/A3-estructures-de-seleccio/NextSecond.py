"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   L'usuari introdueix una hora amb tres enters
   (hores, minuts i segons). 
   Imprimeix l'hora que serà al cap d'un segon.
"""
"""
import datetime

usuariTime = input("Introdueix una hora amb 3 enters (hh mm ss): ")
hores, minuts, segons = map(int, usuariTime.split(" "))
nextSecond = datetime.time(hores, minuts, segons+1)
print("En un segon será:", nextSecond)
"""

horaEntrada = input()
horaSeparada = horaEntrada.split(" ")
hora = int(horaSeparada[0])
min = int(horaSeparada[1])
seg = int(horaSeparada[2])

esHoraCorrecta = (0<=hora<=23 and 0<=min<=59 and 0<=seg<=59)
if esHoraCorrecta:
   seg = seg+1
   if seg == 60:
      seg = 0
      min = min+1
      if min == 60:
         min = 0
         hora = hora+1
         if hora == 24:
            hora = 0
   print(hora, min, seg, sep=":")
else:
   print("Format hora incorrecte!")