"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   L'usuari escriu un enter.
   Imprimeix bitllet vàlid si existeix
   un bitllet d'euros amb la quantitat entrada,
   bitllet invàlid en qualsevol altre cas.
"""
uBitllet = int(input("Escriu un enter: "))
bitllets = {5,10,20,50,100}

if uBitllet in bitllets:
    print("Bitllet vàlid.")
else:
    print("Bitllet invàlid.")