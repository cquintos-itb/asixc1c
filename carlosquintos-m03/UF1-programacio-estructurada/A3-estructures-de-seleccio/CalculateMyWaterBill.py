"""
   Carlos Quintos Nores
   30 de març 2023
   ASIXc1C M03 UF1 A3
   Descripció:
   L'usuari introdueix la lletra del tipus d'habitatge i número de m^3 d'aigua gastats.
   Mostrar per pantalla el preu total.
"""

# region Variables and constants
quotaFixa = {'A':2.60,'B':6.76,'C':7.66,'D':11.85,'E':12.78,'F':18.26,'G':29.59,'H':42.79,'I':64.77}
cabalNominal = {'A':0.25,'B':0.33,'C':0.40,'D':0.50,'E':0.63,'F':1.00,'G':1.60,'H':2.50,'I':4.00}
# endregion

# region Main program
lletra_input = input()
aigua_gastada = float(input())
total = aigua_gastada*cabalNominal[lletra_input]
print(quotaFixa[lletra_input]+total)
# endregion