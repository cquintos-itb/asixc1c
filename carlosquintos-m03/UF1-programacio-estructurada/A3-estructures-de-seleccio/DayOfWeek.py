"""
   Carlos Quintos Nores
   30 de març 2023
   ASIXc1C M03 UF1 A3
   Descripció:
   Donat un enter, mostrar el dia de la setmana amb text (dilluns, dimarts, dimecres…).
"""

# region Variables & Constants
dies = (1,'dilluns','dimarts','dimecres','dijous','divendres','dissabte','diumenge')
user_input = int(input())
# endregion

# region Main Program
print(dies[user_input])
# endregion