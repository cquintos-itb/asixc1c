"""
   Carlos Quintos Nores
   03 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   Demana dos enters a l'usuari i imprimeix
   el valor més gran.
"""
vUno=int(input("Valor 1: "))
vDos=int(input("Valor 2: "))
nMax=max(vUno,vDos)
print(nMax)