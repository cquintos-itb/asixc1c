"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   L'usuari introdueix un any.
   Indica si és de traspàs printant "2020 és any
   de traspàs" o "2021 no és any de traspàs".
"""

# region Main program
any=int(input("\nEntrada de mostra: "))
if any > 1582:
   if any % 4 == 0 & (any % 400 == 0):
      print(f"\n{any} és any de traspàs")
   elif any % 100 != 0:
      print(f"\n{any} no és any de traspàs")
else:
   print("No dins del període del calendari gregorià")
# endregion