"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   L'usuari escriu un valor que representa una nota.
   Imprimeix "Excel·lent", "Notable", "Bé", "Suficient",
   "Suspès", "Nota invàlida" segons  la nota
   numèrica introduïda.
"""

# region Main program
nota = float(input("\nEscriu un valor: "))
if nota <=10 and nota >=9:
    print("\nExcel·lent!\n")
elif nota <=8 and nota >=7:
    print("\nNotable\n")
elif nota <= 7 and nota >=6:
    print("\nBé\n")
elif nota <= 6 and nota >=5:
    print("\nSuficient\n")
elif nota <=5 and nota >=0:
    print("\nSuspès\n")
else:
    print("\nNota invàlida\n")
# endregion