"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   Mostrar el valor absolut d'un enter entrat per l'usuari.
"""

# region Variables & Constants
uNumber = int(input("Introdueix un enter: "))
# endregion

# region Main Program
print(abs(uNumber))
# endregion