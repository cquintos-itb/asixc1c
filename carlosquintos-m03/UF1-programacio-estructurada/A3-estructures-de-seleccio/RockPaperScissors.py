"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   Volem fer el joc de pedra, paper, tisora.
   L'usuari introdueix dos enters (1) pedra,
   (2) paper, (3) tisora.
   Imprimeix per pantalla Guanya el primer,
   Guanya el segon, Empat segons els enters introduïts.
"""

option1, option2 = int(input("1-Pedra\n2-Paper\n3-Tisora\n\nTria l'opció 1: ")), int(input("Tria l'opció 2: "))

if option1==1 and option2==2:
    print("\nGuanya el segon")
elif option1==1 and option2==3:
    print("\nGunya el primer")
elif option1==2 and option2==1:
    print("\nGunya el segon")
elif option1==2 and option2==3:
    print("\nGuanya el segon")
elif option1==3 and option2==1:
    print("\nGuanya el segon")
elif option1==3 and option2==2:
    print("\nGuanya el primer")
else:
    print("Error!")