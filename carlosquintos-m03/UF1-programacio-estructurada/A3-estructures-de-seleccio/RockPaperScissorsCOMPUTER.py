"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   Volem fer el joc de pedra, paper, tisora.
   Aquesta vegada jugant 1 usuari contra l'ORDINADOR!
   L'usuari introdueix només un enter:
   (1) pedra, (2) paper, (3) tisora.
   Imprimeix per pantalla Guanya el primer,
   Guanya el segon, Empat segons els enters introduïts.
"""

import random

options=["pedra", "paper", "tisora"]
botPlayer=random.choice(options)

# MAIN PROGRAM
print("\n#########################\n# Pedra, Paper, Tisores #\n#########################\n")

myTry = int(input("1-Pedra\n2-Paper\n3-Tisora\n\nTria: "))

if myTry == botPlayer:
    print("\nHeu empatat!\n")
elif myTry == 1:
    if botPlayer == "tisora":
        print(f"\nL'ordinador ha triat {botPlayer}, has guanyat!\n")
    else:
        print(f"\nL'ordinador ha triat {botPlayer}, has perdut\n")
elif myTry == 2:
    if botPlayer == "pedra":
        print(f"\nL'ordinador ha triat {botPlayer}, has guanyat!\n")
    else:
        print(f"\nL'ordinador ha triat {botPlayer}, has perdut\n")
elif myTry == 3:
    if botPlayer == "paper":
        print(f"\nL'ordinador ha triat {botPlayer}, has guanyat!\n")
    else:
        print(f"\nL'ordinador ha triat {botPlayer}, has perdut\n")