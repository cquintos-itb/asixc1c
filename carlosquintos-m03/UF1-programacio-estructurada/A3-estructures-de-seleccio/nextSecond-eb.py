"""
   Carlos Quintos Nores
   03 de Nov. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   Mostrar l'hora sistema, en comptes de demanar-li a l'usuari.
   Demanar en quants segons es vol incrementar l'hora,
   i mostrar per pantalla "quina seria" l'hora
   (No es demana modificar l'hora del sistema).
"""

# region Imports
import datetime
# endregion

# region Main program
now = datetime.datetime.now()
currentTime = now.strftime("%H:%M:%S")
print(currentTime)
userTime = int(input("Quants segons vols incrementar? "))
print(now.hour, now.minute, now.second+userTime, sep=":")
# endregion