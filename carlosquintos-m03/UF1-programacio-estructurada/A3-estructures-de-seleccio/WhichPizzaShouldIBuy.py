"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   Volem comparar quina pizza és més gran,
   entre una rectangular i una rodona.
   L'usuari entra el diàmetre d'una pizza rodona.
   L'usuari entra els dos costats de la pizza rectangular.
   Imprimeix "Compra la rodona" si la pizza rodona
   és més gran, o "Compra la rectangular" en qualsevol
   altre cas.
"""
pDiametre = float(input("Introdueix el diàmetre de la pizza A: "))
costatA, costatB = float(input("Introdueix el costat 1 de la pizza B: ")), float(input("Introdueix el costat 2 de la pizza B: "))

from math import pi
circumferencia = round(pDiametre * pi)
area = round(costatA * costatB)

if circumferencia > area:
    print("Compra la rodona.")
else:
    print("Compra la rectangular")