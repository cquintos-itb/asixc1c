"""
    Carlos Quintos Nores
    20 de Oct. 2022
    ASIXc1C M03 UF1 A3
    Descripió:
    L'usuari introdueix una quantitat d'euros.
    Mostra per pantalla el número mínim de cada tipus de bitllets i monedes per tenir aquesta quantitat
    Input
        603.25
    Output
        3 bitllets de 500€
        1 bitllet de 100€
        3 monedes de 1€
        2 monedes de 10cèntims
        1 moneda de 5cèntims
"""