"""
   Carlos Quintos Nores
   03 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   El programa ha de mostrar un dels dos missatges
   possibles, que són Any de traspàs o Any comú,
   segons el valor ingressat. (Només llegir el número
   d'any, per facilitar l'algoritme).
"""

# region Main program
any=int(input("Entrada de mostra: "))
if any > 1582:
   if any % 4 == 0 & (any % 400 == 0):
      print("Es bisiesto")
   elif any % 100 != 0:
      print("No es bisiesto")
else:
   print("No dins del període del calendari gregorià")
# endregion