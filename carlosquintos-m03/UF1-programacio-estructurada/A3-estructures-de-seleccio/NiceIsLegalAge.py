"""
   Carlos Quintos Nores
   20 de Oct. 2022
   ASIXc1C M03 UF1 A3
   Descripció:
   L'usuari escriu un enter amb la seva edat i
   s'imprimeix ets major d'edat si és major d'edat.
"""
edad = int(input("Escriu la teva edad: "))
if edad >= 18:
    print("Ets mayor d'edat.")
else:
    print("No ets mayor d'edat.")