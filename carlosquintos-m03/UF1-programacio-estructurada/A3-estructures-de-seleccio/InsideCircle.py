"""
    Carlos Quintos Nores
    20 de Oct. 2022
    ASIXc1C M03 UF1 A3
    Descripió:
    Fes un programa que avaluï si les coordenades d'un punt (x,y) es troben dins la circumferència descrita pel centre(a,b) i el seu radi r.
    L'usuari introdueix els 5 valors en l'ordre x, y, a, b, r
    Imprimeix el punt és dins de la circumferència o el punt és fora
"""
