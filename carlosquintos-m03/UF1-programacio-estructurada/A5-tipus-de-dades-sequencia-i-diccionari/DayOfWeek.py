"""
   Carlos Quintos Nores
   19 de Des. 2022
   ASIXc1C M03 UF1 A5
   Descripció:
   Donat un enter, mostrar el dia de la setmana amb text
   (dilluns, dimarts, dimecres…),
   tenint en compte que dilluns és el 0.
   Els dies de la setmana es guarden en una llista.
"""

# region Variables & Constants
weekdays = [ "dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabta", "diumenje" ]
# endregion

# region Main Program
userInput = int(input(""))
print(weekdays[userInput])
# endregion