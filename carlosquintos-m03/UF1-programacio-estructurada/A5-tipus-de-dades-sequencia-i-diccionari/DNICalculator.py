"""
   Carlos Quintos Nores
   09 de Gen. 2023
   ASIXc1C M03 UF1 A5
   Descripció:
   L’objectiu d’aquest exercici és crear un programa
   que utilitzi un diccionari per calcular el DNI. 
"""
#region Variables and Constants
LIST = ('T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E')
#endregion

#region Main Program
dniInput = int(input(""))
print(LIST[dniInput%23])
#endregion