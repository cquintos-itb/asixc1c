"""
   Quintos, Sallent, Santiago
   06 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Escriu un programa que llegeixi 5 enters. 
   El primer i el segon creen un rang, el tercer 
   i el quart creen un altre rang. Mostra True si 
   el cinquè valor està entre els dos rangs, si no False.
"""

# region Imports
from traceback import print_tb
# endregion

# region Main program
vUno, vDos, vTres, vCuatro, vCinco = int(input("Enter 1: ")), int(input("Enter 2: ")), int(input("Enter 3: ")), int(input("Enter 4: ")), int(input("Enter 5: "))
range1=range(vUno, vDos)
range2=range(vTres, vCuatro)
print(vCinco in range1 and vCinco in range2)
# endregion