"""
   Carlos Quintos Nores
   03 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   L'usuari escriu dos valors i s'imprimeix true
   si el primer és més gran que el segon i false
   en qualsevol altre cas.
"""

# region Variables and constants
v1=int(input("Introdueix valor 1: "))
v2=int(input("Introdueix valor 2: "))
# endregion

# region Main program
print(v1>v2)
# endregion