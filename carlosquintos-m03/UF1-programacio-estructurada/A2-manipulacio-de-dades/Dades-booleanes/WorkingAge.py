"""
   Quintos, Sallent, Santiago
   06 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Escriu un programa que llegeixi l'edat
   de l'usuari i mostri si té edat per treballar,
   l'edat mínima per treballar legalment és 16
   i suposarem l'edat màxima als 65.
"""

# region Variables and constants
tedat=int(input("Diga's la teva edat: "))
# endregion

# region Main program
print((tedat>=16) & (tedat<=65))
# endregion