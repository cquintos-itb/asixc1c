"""
   Carlos Quintos Nores
   03 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   L'usuari escriu un enter amb la seva edat
   i s'imprimeix true si és major d'edat, i false
   en qualsevol altre cas.
"""

# region Main program
edad=int(input("Quina edad tens?: "))
print(edad>=18)
# endregion