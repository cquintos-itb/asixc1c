"""
   Carlos Quintos Nores
   03 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   L'usuari escriu un enter i s'imprimeix true 
   si existeix un bitllet d'euros amb la quantitat 
   entrada, false en qualsevol altre cas.
"""

# region Variables and constants
bllet=int(input("Escriu un enter: "))
Bllet={5,10,20,50,100}
# endregion

# region Main program
print(True if bllet in Bllet else False)
# endregion