"""
   Quintos, Sallent, Santiago
   06 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Expliqueu que passa a:
        adn = "ATGAACTGTGC"
        adn=adn.replace('A', 'a')
        adn=adn.replace('T', 'A')
        adn=adn.replace('a', 'T')
"""
# adn.replace('A', 'a') canvia les "A" per "a".
# adn.replace('T', 'A') canvia les "T" per "A".
# adn=adn.replace('a', 'T') canvia les "a" per "T".

adn = "ATGAACTGTGC"
adn=adn.replace('A', 'a')
adn=adn.replace('T', 'A')
adn=adn.replace('a', 'T')
print(adn)