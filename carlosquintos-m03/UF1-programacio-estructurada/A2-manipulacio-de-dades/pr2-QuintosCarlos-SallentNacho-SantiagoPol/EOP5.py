"""
   Quintos, Sallent, Santiago
   06 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Quin dels noms de les variables és incorrecte:
   spam, spam43, spam_43, 43spam, spAm
"""
# La variable 43spam és incorrecte perque no es pot
# crear una variable començant amb un número.