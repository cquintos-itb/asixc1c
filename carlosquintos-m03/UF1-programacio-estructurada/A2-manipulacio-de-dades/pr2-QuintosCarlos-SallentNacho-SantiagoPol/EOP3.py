"""
 Quintos, Sallent, Santiago
   10 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Usant un editor de textos, genereu una funció que calculi el pas de graus Celsius a Fahrenheit amb la fórmula: F = (9 / 5) * C + 32
"""
grados_c=float(input("Ingresa cuantos grados Celsius hace hoy y te los convierto a grados Fahrenheit: "))
grados_f=grados_c * 1.8 + 32
print(grados_f)