"""
   Quintos, Sallent, Santiago
   06 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   EOP 1 
   Corregir el següent codi, perquè compili i funcioni correctament. 
   El resultat esperat és, “Hola2”
   assert = 'Hola'
   print assert + '2'
"""
#Codi correcte:
xassert = 'Hola'
print (xassert + '2')
