"""
   Quintos, Sallent, Santiago
   06 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Demana una paraula per teclat i mostrar-la 
   per pantalla, canviar les vocals per als numèrics
   1, 2, 3, 4 o 5.
   Tenint en compte, que la lletra a i A és l'1,
   consecutivament fins a la lletra u i U que és el 5.
"""
frase=str(input("Escribe la frase donde quieres cambiar vocales por números: "))
replace=frase.replace("a","1").replace("e","2").replace("i","3").replace("o","4").replace("u","5").replace("A","1").replace("E","2").replace("I","3").replace("O","4").replace("U","5")
print(replace)