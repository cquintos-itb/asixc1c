"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF1 A2
    Descripció:
"""

# region Main program
num1 = int(input())
num2 = int(input())
print(f"Suma: {num1+num2}, Resta: {num1-num2}, Divisió: {num1/num2}")
# endregion