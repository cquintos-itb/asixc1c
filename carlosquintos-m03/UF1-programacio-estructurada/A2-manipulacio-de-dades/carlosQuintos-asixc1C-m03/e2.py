"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF1 A2
    Descripció:
    Donada la base i altura d'un rectangle, escriure el programa que calcula
    i mostra el seu perímetre i àrea.
"""

# region Main program
base = float(input("Entra base: "))
altura = float(input("Entra altura: "))
print(f'"La area és: {base*altura}, i el perimetres és: {2*(base+altura)}"')
# endregion