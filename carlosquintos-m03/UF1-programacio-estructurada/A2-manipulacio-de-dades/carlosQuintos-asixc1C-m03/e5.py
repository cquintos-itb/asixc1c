"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF1 A2
    Descripció:
    Realitza un programa que demani a l'usuari graus Fahrenheit i posteriorment els mostri com graus Celsius.
    La fórmula per a la conversió és:
    C = (F-32)*5/9
"""

# region Main program
graus_fahrenheit = float(input())
print(f"En graus Celsius és: {(graus_fahrenheit-32)*5/9:.2f}")
# endregion