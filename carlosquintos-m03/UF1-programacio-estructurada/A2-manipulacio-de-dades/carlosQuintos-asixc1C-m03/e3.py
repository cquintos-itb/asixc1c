"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF1 A2
    Descripció:
    Donats els catets a i b d'un triangle rectangle,
    escriure el codi Python que calcula la seva hipotenusa.
    Recordar que la hipotenusa es calcula de la següent manera (Teorema de Pitàgores):
    c2 = a2 + b2 , on c és la hipotenusa. Per tant  c = √(a2 + b2)
"""

# region Imports
from math import sqrt
# endregion

# region Main program
a = int(input())
b = int(input())
print(f"La hipotenusa és {sqrt(a**2+b**2):.2f}")
# endregion