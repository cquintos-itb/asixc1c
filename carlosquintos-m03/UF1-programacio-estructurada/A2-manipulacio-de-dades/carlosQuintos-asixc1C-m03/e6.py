"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF1 A2
    Descripció:
    Escriu un programa que demani a l'usuari entrar tres nombres
    i després escrigui a la pantalla la seva mitjana.
"""

# region Imports
# endregion

# region Variables and constants
# endregion

# region Functions
# endregion

# region Main program
num1, num2, num3 = input().split()
# endregion