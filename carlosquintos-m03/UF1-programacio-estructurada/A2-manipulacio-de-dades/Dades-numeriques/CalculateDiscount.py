"""
   Carlos Quintos Nores
   03 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Llegeix el preu original i el preu actual
   i imprimeix el descompte (en %).
"""

# region Variables and constants
pOri=float(input("Escriu preu Original: "))
pAct=float(input("Escriu preu Actual: "))
# endregion

# region Main program
print(f"{int((pAct*100)/pOri)}%")
# endregion