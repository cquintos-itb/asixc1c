"""
   Quintos, Sallent, Santiago
   06 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Llegeix el diàmetre d'una pizza rodona
   i imprimeix la seva superfície.
   Pots usar Math.PI per escriure el valor de Pi.
"""

# region Imports
from math import pi
# endregion

# region Main program
radi = float(input("Diga'm el radi: "))
print(f"La seva superficie és de {round(pi*radi**2,2)} cm²")
# endregion