"""
    Carlos Quintos Nores
    03 de Oct. 2022
    ASIXc1C M03 UF1 A2
    Descripció:
    Llegeix un valor amb decimals i imprimeix el doble.
"""

# region Main program
num_decimal = float(input())
print(num_decimal*2)
# endregion