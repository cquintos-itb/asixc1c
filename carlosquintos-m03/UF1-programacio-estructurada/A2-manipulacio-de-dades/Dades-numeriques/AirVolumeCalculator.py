"""
   Quintos, Sallent, Santiago
   06 de Oct. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   Per poder fer un estudi de la ventilació d'una aula necessitem poder calcular la quantitat d'aire que hi cap en una habitació. Llegeix les 3 dimensions de l'aula i mostra per pantalla quin volum té.
"""

# region Variables and constants
Lengh=float(input("Digue'm la llargada de l'aula: "))
High=float(input("Digue'm la alçada de l'aula: "))
Large=float(input("Digue'm la amplada de l'aula: "))
Volume=Lengh*High*Large
# endregion

# region Main program
print(f"En esta aula caben {Volume:.2f} l²")
# endregion