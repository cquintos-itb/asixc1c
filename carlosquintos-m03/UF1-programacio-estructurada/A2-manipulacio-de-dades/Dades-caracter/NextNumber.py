"""
   Carlos Quintos Nores
   29 de Sep. 2022
   ASIXc1C M03 UF1 A2
   Descripció:
   L'usuari escriu un número enter.
   "Printa" per pantalla el número següent.
   Per exemple, si entro el 4.
   El programa ha de mostrar la frase "Després ve el 5".
"""

# region Variables and constants
# numInt= Et demana un número enter i guarda el seu valor.
numInt=int(input("Entra un número enter: "))
# endregion

# region Main program
# Aquí printa el valor de "numInt" més un "1".
print(f"El següent número és: {numInt+1}")
# endregion