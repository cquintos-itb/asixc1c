"""
   Carlos Quintos Nores
   23 de Gen. 2023
   ASIXc1C M03 UF1 A3
   Descripció:
   MATRIU Vores.
"""

# region Variables & Constants
uns = "1"
ceros = "0"
# endregion

# region Main Program
filesUsuari=int(input("Files: "))
columnesUsuari=int(input("Columnes: "))
diferencia = filesUsuari-columnesUsuari
dif_lateral = round(diferencia/2)

for i in range(dif_lateral):
    print(uns*filesUsuari)
for i in range(columnesUsuari):
    print(uns*dif_lateral, end="")
    print(ceros*columnesUsuari, end="")
    print(uns*dif_lateral)
for i in range(dif_lateral):
    print(uns*filesUsuari)
# endregion