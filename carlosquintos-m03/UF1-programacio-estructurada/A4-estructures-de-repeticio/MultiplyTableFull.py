"""
   Carlos Quintos Nores
   24 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Imprimeix les taules de multiplicar en forma de taula.
"""

# region VARIABLES & CONSTANTS
MIDA = 9
# endregion

# region MAIN PROGRAM
for x in range (1, MIDA+1):
    print()
    for y in range (1, MIDA+1):
        print("\t", x*y, end="")
# endregion