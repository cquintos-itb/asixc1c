"""
   Carlos Quintos Nores
   28 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Escriu un programa que demani per teclat lletres,
   d’una en una, fins que l’usuari entri un espai en blanc.
   Per cada lletra entrada, el programa comprovarà 
   si es tracta d’una vocal. Si ho és, escriurà en pantalla
   ‘VOCAL’. En cas contrari, ‘NO VOCAL’
   (aquest cas inclou caràcters que no siguin lletres).
"""

# region Variables & Constants
VOCALS = ["a", "e", "i", "o", "u"]
# endregion

# region Main Program
while True:
    userType = input("Introdueix una lletra: ")
    if len(userType) > 1:
        continue
    if userType == "" or userType == " ":
        break
    if userType.lower() in VOCALS:
        print("VOCAL")
    else:
        print("NO VOCAL")
# endregion