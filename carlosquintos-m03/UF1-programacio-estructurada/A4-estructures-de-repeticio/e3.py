"""
   Carlos Quintos Nores
   28 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Escriu un programa que demani números per teclat fins que 
   s'introdueix un zero.
   El programa haurà d'imprimir la suma i la mitjana
   de tots els números introduïts.
"""

# region Variables & Constants
num = 0
# endregion

# region Main Program
while True:
    userType = int(input("Introdueix un número: "))
    num = num+userType
    if userType == 0:
        break
print(f"La suma és {num} i la mitjana és", int(num/2))
# endregion