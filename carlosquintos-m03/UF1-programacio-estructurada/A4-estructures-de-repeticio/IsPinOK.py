"""
   Carlos Quintos Nores
   17 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Programa que demana a l'usuari una contrasenya,
   de manera repetitiva mentre que no introdueixi “asdasd”.
   Quan finalment escrigui la contrasenya correcta,
   li dirà “Benvingut” i acabarà el programa.
"""
# region VARIABLES & CONSTANTS
pin = 1234
contador = 0
userPin = int(input("Intruzca el PIN: "))
# endregion

# region MAIN PROGRAM
while userPin != pin:
   print("PIN Incorrecto!")
   userPin = int(input("Intruzca el PIN: "))
   contador = contador+1
   if contador == 2:
      print("La has cagao bacalao!")
      break
else:
   print("Bienvenido!")
   print("Programa terminado")
# endregion