"""
   Carlos Quintos Nores
   27 de Oct. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Implementa un programa que demani un número
   i calculi el seu factorial.
"""

# region Variables & Constants
factorial = 1
# endregion

# region Main Program
num = int(input("Entra un número: "))
if num < 0:
   print("Número no válid!")
elif num == 0:
   print("El factorial de 0 és 1")
else:
   for i in range(1, num+1):
      factorial = factorial*i
   print(f"El factorial de {num} és {factorial}")
# endregion