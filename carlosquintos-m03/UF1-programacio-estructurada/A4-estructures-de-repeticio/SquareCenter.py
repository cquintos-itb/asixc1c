"""
   Carlos Quintos Nores
   24 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Fes una funció que dibuixi dos quadrats concèntrics.
   L'usuari introdueix dos valors, el primer defineixen
   el costat del quadrat extern que pintarem amb o,
   el segon defineix el costat del segon quadrat.
   Aquest segon quadrat el pintarem amb *.
"""

# region VARIABLES & CONSTANTS
CIRCULITO = "o"
ASTEROIDE = "*"
# endregion

# region MAIN PROGRAM
userInput = input("")
costats = userInput.split(" ")
costatA = int(costats[0])
costatB = int(costats[1])
diferencia = costatA-costatB
dif_lateral = round(diferencia/2)

for i in range(dif_lateral):
    print(CIRCULITO*costatA)
for i in range(costatB):
    print(CIRCULITO*dif_lateral, end="")
    print(ASTEROIDE*costatB, end="")
    print(CIRCULITO*dif_lateral)
for i in range(dif_lateral):
    print(CIRCULITO*costatA)
# endregion