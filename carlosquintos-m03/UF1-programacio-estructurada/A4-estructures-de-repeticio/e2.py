"""
   Carlos Quintos Nores
   28 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Crea una aplicació que permet endevinar un número.
   L'aplicació genera un nombre “aleatori” de l'1 al 100.
"""

# region Variables & Constants
import random
magicNumber = random.randint(1,100)
contador = 0
# endregion

# region Main Program
for i in range(10):
   contador = contador+1
   urTurn = int(input("Posa un número: "))
   if urTurn == magicNumber:
      print(f"\nHas necesitat {contador} intents")
   elif urTurn > magicNumber:
      print("El número és massa gran")
   elif urTurn < magicNumber:
      print("El número és massa petit")
# endregion