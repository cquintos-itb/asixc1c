"""
   Carlos Quintos Nores
   21 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Programa per crear una Python a mida.
   Cal demanar quina mida ha de tenir la serp.
   Tot seguit mostrar-la per pantalla.
   Es valorarà el fet de que el cos faci siga sagues.
"""

# region VARIABLES & CONSTANTS
CAP= "....\...../ ...."
ULLS= "...╚⊙ ⊙╝..."
COS = "═(███)═"
CUA =  "  ═V═  "
# endregion

# region MAIN PROGRAM
mida = int(input("Quina mida vols?: "))
print(CAP)
print(ULLS)
for x in range (mida):
    if x%2==0:
        print(" ",COS)
    else:
        print(COS)
print(CUA)
# endregion