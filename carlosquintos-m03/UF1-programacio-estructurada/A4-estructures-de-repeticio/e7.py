"""
   Carlos Quintos Nores
   28 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Realitza un programa que mostri la taula de multiplicar
   d'un número introduït per teclat.
"""

# region Variables & Constants
TAULA = 12
# endregion

# region Main Program
userInput = int(input("Introdueix un número: "))
for i in range(1, TAULA+1):
    print(i*userInput, end="|")
# endregion