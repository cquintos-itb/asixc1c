"""
   Carlos Quintos Nores
   12 de Des. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Escriu un programa que donat dos números entrats per teclat,
   base i exponent (tots dos enters), tregui per pantalla el resultat
   de la potència. No pots fer servir l'operador de potència ni la
   funció “pow” del mòdul “math”. 
"""

# region Functions & Variables
# endregion
potencia = 0
# region Main Program
while True:
    userInput = input("Introdueix la base i l'exponent: ")
    splitter = userInput.split(" ")
    base = int(splitter[0])
    exponent = int(splitter[1])
    if base or exponent == 0:
        break
    elif exponent < 0:
        continue
    else:
        for i in range(exponent):
            base*potencia
print(potencia)
# endregion