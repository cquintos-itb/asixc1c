"""
   Carlos Quintos Nores
   27 de Oct. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Mostra per pantalla tots els números fins
   a un enter entrat per l'usuari.
"""

# region MAIN PROGRAM
enterUsuari = int(input("Entra un enter: "))
for x in range(1, enterUsuari+1):
    print(x)
# endregion