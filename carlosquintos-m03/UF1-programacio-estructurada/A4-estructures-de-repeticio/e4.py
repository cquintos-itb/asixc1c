"""
   Carlos Quintos Nores
   28 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Implementa un programa que demani números per teclat.
   Prèviament, el programa ha de demanar
   la quantitat de números que l’usuari vol introduir.
   Un cop introduïts tots els números,
   el programa informarà de quants són més grans que 0,
   menors que 0 i iguals a 0.
"""

# region Variables & Constants
goodVibes=0
sadVibes=0
zeros=0
# endregion

# region Main Program
#numberAssign = int(input("Quina quantitat de números vol introduir?: "))
#for i in range(numberAssign):
numberInput=input("Escriu 10 números sencers (a b c d ...): ")
splitter = numberInput.split(" ")

for i in range(10):
    numero = int(splitter[i])
    if numero > 0:
        goodVibes += 1
    elif numero == 0:
        zeros += 1
    elif numero < 0:
        sadVibes += 1
print(f"Positius: {goodVibes}, Negatius: {sadVibes}, Zeros: {zeros}")
# endregion