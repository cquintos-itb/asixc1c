"""
   Carlos Quintos Nores
   28 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Escriu un programa que demani per teclat dos números
   i a continuació imprimeixi tots els nombres parells
   entre tots dos.
"""

# region Variables & Constants
# endregion

# region Main Program
userInput = input("Introdueix 2 números: ")
splitting = userInput.split(" ")
numOne = int(splitting[0])
numTwo = int(splitting[1])

for i in range(numOne, numTwo):
    if i % 2 == 0:
        print(i)
# endregion