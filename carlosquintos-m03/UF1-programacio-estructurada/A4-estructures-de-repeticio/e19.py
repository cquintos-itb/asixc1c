"""
    Carlos Quintos Nores
    28 de Nov. 2022
    ASIXc1C M03 UF1 A4
    Descripció:
    Escriu un programa que mostri el següent menú d’opcions:
        1. Literatura
        2. Cinema
        3. Música
        4. Videojocs
        5. Sortir
"""

# region Main Program
import time

while True:
    print("\n1. Literatura")
    print("2. Cinema")
    print("3. Música")
    print("4. Videojocs")
    print("5. Sortir\n")
    choise = input("Tria una opció: ")
    if choise in ('1', '2', '3', '4', '5'):
        if choise == '1':
            print("\nHas triat Literatura\n")
            print("- Frankenstein, by Mary Shelley")
            print("- Dracula, by Bram Stoker")
            print("- Flower in the Attic, by V.C")
            time.sleep(1)
        elif choise == '2':
            print("\nHas triat Cinema\n")
            print("- Star Wars: Episode VII - The Force Awakens (2015)")
            print("- Avengers: Endgame (2019)")
            print("- Spider-Man: No Way Home (2021)")
            print("- Avatar (2009)")
            print("- Top Gun: Maverick (2022)")
            print("- Black Panther (2018)")
            print("- Avengers: Infinity War (2018)")
            print("- Titanic (1997)")
            time.sleep(1)
        elif choise == '3':
            print("\nHas triat Música\n")
            print("- Sandy Christmas 2, by Sami Thompson")
            print("- Act in Good Faith, by Alina Smolina")
            print("- I don't know what christmas is (but christmastime is here), by Old 97's")
            print("- All I want for Christmas Is You, by Mariah Carey")
            time.sleep(1)
        elif choise == '4':
            print("\nHas triat Videojocs\n")
            print("- Red Dead Redemption 2")
            print("- Overwatch 2")
            print("- Mario Kart 8 + Deluxe")
            print("- PUBG")
            print("- Wii Sports")
            time.sleep(1)
        elif choise == '5':
            print("\n[+] Adeu!\n")
            break
    else:
        print("\n[!] Opció no valida\n")
        time.sleep(1)
# endregion