"""
   Carlos Quintos Nores
   29 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Implementa un programa que demani dos números:
   un serà el límit inferior d’un interval i l’altre el límit superior.
   Si el límit inferior és més gran que el superior ho ha de tornar a demanar.
"""

# region Variables & Constants
suma = 0
# endregion

# region Main Program
while True:
   userInput = input("Introdueix 2 números: ")
   splitting = userInput.split(" ")
   numOne = int(splitting[0])
   numTwo = int(splitting[1])
   suma = suma+(numOne+numTwo)
   if numOne>numTwo:
      continue
   if numOne or numTwo == 0:
      break
   print(suma)
# endregion