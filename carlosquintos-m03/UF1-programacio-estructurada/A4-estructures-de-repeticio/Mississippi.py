"""
   Carlos Quintos Nores
   24 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Agrega la paraula Mississipí a un número
   en comptar els segons en veu alta fa que
   soni més pròxim al rellotge, i per tant
   "un Mississipí, dos Mississipí, tres Mississipí"
   prendrà aproximadament uns tres segons reals de temps.
   Sovint ho usen els nens que juguen a l'amagatall
   per a assegurar-se que el cercador faci un comptatge honest.
"""

# region VARIABLES & CONSTANTS
import time
CONT = 5
# endregion

# region MAIN PROGRAM
for x in range (1, CONT+1):
    print(x,"Mississipí")
    time.sleep(1)
print('"Llest o no, aquí vaig!"')
# endregion