"""
   Carlos Quintos Nores
   21 de Nov. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Programa entre en bucle fins que
   es complexi una condició. 
"""

# region VARIABLES & CONSTANTS
sortir = False
# endregion

# region MAIN PROGRAM
while not sortir:
    answer = input("Vols sortir?(y/n): ")
    if answer.lower() == "y":
        sortir = True
print("Programa finalitzat")
# endregion