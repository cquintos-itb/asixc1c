"""
   Carlos Quintos Nores
   27 de Oct. 2022
   ASIXc1C M03 UF1 A4
   Descripció:
   Printa per pantalla 100 cops es número 1
"""

# region MAIN PROGRAM
for numberOne in range(100):
    print("Es número 1")
# endregion