"""
   Carlos Quintos Nores
   22 de Sep. 2022
   ASIXc1C M03 UF1 A1
   Descripció:
   Estructura d'un programa informàtic - GIT - Algoritmes
"""

# Programa que demana l'edat i diu si ets major d'edat.
edat=int(input("Quina edat tens? "))
if edat>=18:
    print("Ets major d'edat")
print("Programa Finalitzat")