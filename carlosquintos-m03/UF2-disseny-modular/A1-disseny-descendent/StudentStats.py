"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF2 A1
    Descripció:
    Després de fer un exàmen un professor vol veure l'estat general de la classe. Vol coneixer la nota mínima, la màxima i la mitjana dels seus alumnes. Les notes son entrades per teclat fins que hi hagi un valor -1. Per tant la quantitat de notes és indeterminada, i dependrà de cada execució del programa
    Fes un petit programa que li solucioni la tasca (les notes de l'exàmen no tenen decimals).
        IMPORTANT: no es poden fer servir les funcions de Python. Cal declarar funcions.
    Input
        5 3 4 7 8 -1
    Output
        Nota mínima:  3
        Nota màxima:  8
        Nota mitjana: 5.4
"""

# region Imports
# endregion

# region Variables and constants
# endregion

# region Main program
# endregion