"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF2 A1
    Descripció:
    Fes un programa (en el fitxer CheapestPrice) que donada la llista de preus
    (sense decimals) de tot de productes, retorni el preu del producte més baix.
        IMPORTANT: no es poden fer servir les funcions de Python. Cal declarar funcions.
    Input
        5 3 4 7 8 -1
    Output
        El producte més econòmic val: 3€
    Pista
        numeros = input("").split()
"""

# region Imports
# endregion

# region Variables and constants
# endregion

# region Main program
# endregion