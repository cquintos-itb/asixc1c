"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF2 A1
    Descripció:
    Implementa un programa que simuli un llum, amb les accions:
    * TURN ON: Encén el llum
    * TURN OFF: Apaga el llum
    * TOGGLE: Canvia l'estat del llum
    Després de cada acció mostra si el llum està encesa.
"""

# region Imports
# endregion

# region Variables and constants
# endregion

# region Main program
# endregion