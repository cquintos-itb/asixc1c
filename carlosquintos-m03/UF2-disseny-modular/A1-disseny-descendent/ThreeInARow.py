"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF2 A1
    Descripció:
    Fes un programa que permeti a dos jugadors jugar al tres en ratlla.
    Es juga amb un taulell de 3x3. Cada jugador col·loca una peça al taulell alternativament. Guanya qui és capaç de col·locar-les formant una línia de 3, sigui vertical, horitzontal o diagonal.
    El programa a desenvolupar haurà de mostrar el taulell per pantalla i demanar la tirada al primer jugador.
    El jugador escriurà dos números, el primer correspon a les columnes i el segon a les files. S'anirà demanant el torn alternativament a
    cada jugador fins que algun dels dos faci el tres en ratlla o bé fins que el taulell estigui ple.
    Les fitxes d'un jugador les representarem amb una X i les de l'altre amb una O.
    Al final es mostrarà un missatge dient el resultat de la partida: guanyen les X, guanyen les O, o empat.
    Exemple
    Les línies que comencen per > són les que ha escrit l'usuari
        > 0 0
        X · ·
        · · ·
        · · ·
        > 1 1
        X · ·
        · 0 ·
        · · ·
        > 0 1
        X X ·
        · 0 ·
        · · ·
        > 2 2
        X X ·
        · 0 ·
        · · 0
        > 0 3
        X X X
        · 0 ·
        · · 0
        guanyen les X
"""

# region Imports
# endregion

# region Variables and constants
# endregion

# region Main program
# endregion