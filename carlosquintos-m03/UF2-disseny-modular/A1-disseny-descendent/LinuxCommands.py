"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF2 A1
    Descripció:
    Al ITB s'està creant un nou sistema operatiu, basat el Linux.
    De moment, només té unes poques comandes, donat que és una versió alfa.
"""

# region Imports
import os
# endregion

# region Variables and constants
commands=['touch','grep','cat','fdisk','cmp','dmesg','man','top','hop','halt']
parametres=['--h','--v','--help','--version']
# endregion

# region Main program
commandInput,parametresInput=input("").split()
if commandInput==commands[0] and parametresInput==parametres[0] or parametresInput==parametres[2]:
    print("Usage: touch [OPTION]... FILE...")
elif commandInput==commands[0] and parametresInput==parametres[1] or parametresInput==parametres[3]:
    print("touch (GNU coreutils) 8.32")
# endregion