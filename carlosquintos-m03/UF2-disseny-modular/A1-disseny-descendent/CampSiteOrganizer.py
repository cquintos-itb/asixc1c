"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF2 A1
    Descripció:
    Volem fer un petit programa per gestionar un càmping. Volem tenir controlat quantes parcel·les tenim plenes i quanta gent tenim.
    Quan s'ocupi una parcel·la l'usuari introduirà el número de persones i el nom de la reserva
    ENTRA 2 Maria
    Quan es buidi una parcel·la l'usuari introduirà el nom de la reserva
    MARXA Maria
    Per tancar el programa l'usuari introduirà el nom END
    Cada cop que marxi algú, imprimeix el número total de persones que hi ha, i el número de parcel·les plenes.
    Exemple
    Les línies que comencen per > són les que ha escrit l'usuari
        > ENTRA 2 Maria
        parcel·les: 1
        persones: 2
        > ENTRA 5 Mar
        parcel·les: 2
        persones: 7
        > MARXA Maria
        parcel·les: 1
        persones: 5
        > END
"""

# region Imports
# endregion

# region Variables and constants
# endregion

# region Main program
# endregion