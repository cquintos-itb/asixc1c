"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF2 A1
    Descripció:
    Fes un programa que permeti moure un robot en un pla 2D.
    El robot ha de guardar la seva posició (X, Y) i la velocitat actual. Per defecte, la posició serà (0, 0) i la velocitat és 1. La velocitat indica la distància que recorre el robot en cada acció.
    El robot té les següents accions:
        DALT: El robot es mou cap a dalt (tenint en compte la velocitat).
        BAIX: El robot es mou cap a baix (tenint en compte la velocitat).
        DRETA: El robot es mou cap a la dreta (tenint en compte la velocitat).
        ESQUERRA: El robot es mou cap a l'esquerra (tenint en compte la velocitat).
        ACCELERAR: El robot augmenta en 0.5 la velocitat. La velocitat màxima és 10.
        DISMINUIR: El robot augmenta en 0.5 la velocitat. La velocitat mínima és 0.
        POSICIO: El robot imprimeix la seva posició.
        VELOCITAT: El robot imprimeix la seva velocitat.
    El programa acaba amb l'acció END.
    Exemple
    Les línies que comencen per > són el que ha escrit l'usuari
        > POSICIO
        La posició del robot és (0.0, 0.0)
        > DALT
        > DALT
        > ESQUERRA
        > POSICIO
        La posició del robot és (-1.0, 2.0)
        > ACCELERAR
        > VELOCITAT
        La velocitat del robot és 1.5
        > DALT
        > POSICIO
        La posició del robot és (-1.0, 3.5)
        > END
"""

# region Imports
# endregion

# region Variables and constants
# endregion

# region Main program
# endregion