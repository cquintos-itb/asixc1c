"""
    Carlos Quintos Nores
    31 de març 2023
    ASIXc1C M03 UF2 A1
    Descripció:
    Volem fer un petit programa per guardar la informació dels diferents llibres que té una biblioteca.
    Per introduir els llibres, primer introduirà el número de llibres i la informació de cada llibre (títol, autor, número de pàgines).
    Quan acabi volem que mostri la llista de llibres i el número total, el llibre més curt i el llibre més llarg (en cas d'empat el primer que hagi introduït).
    Input
        3
        Lorem
        Ot Pi
        56
        Ipsum
        Mar Om
        77
        Dolor
        Lin Om
        42
    Output
        Llibres
        ------------
        Lorem - Ot Pi - 56 pàgines
        Ipsum - Mar Om - 77 pàgines
        Dolor - Lin Om - 42 pàgines
        ------------
        Total: 3 llibres
        Llibre més curt: Dolor - Lin Om - 42 pàgines
        Llibre més llarg: Ipsum - Mar Om - 77 pàgines
"""

# region Imports
# endregion

# region Variables and constants
# endregion

# region Main program
# endregion